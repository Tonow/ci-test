package com.innovate.addition;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("add")
public class Add {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getResult(@DefaultValue("0") @QueryParam("operand1") int op1,
                            @DefaultValue("0") @QueryParam("operand2") int op2) {
        int result = op1 + op2;
        return "" + result ;
    }
}
