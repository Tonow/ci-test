
echo 'Waiting for substraction service'
until $(curl --output /dev/null --silent --head --fail http://$SUBSTRACTION_HOST:$SUBSTRACTION_PORT/substraction/sub); do
  echo 'Waiting for substraction service'
  sleep 1
done
echo 'Substraction service found'


echo "Starting integration tests"
curl "http://$SUBSTRACTION_HOST:$SUBSTRACTION_PORT/substraction/sub?operand1=38&operand2=17"


echo "TESTS OK"